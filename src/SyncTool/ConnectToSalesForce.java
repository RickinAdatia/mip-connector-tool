/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SyncTool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import static java.lang.System.exit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;
import org.cometd.bayeux.Message;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Contains Functions that help to interact with the SalesForce account.
 *
 * @author Saranya
 */
public class ConnectToSalesForce {

  // Used to read data from the Input Buffer.
  private BufferedReader INPUT_READER = new BufferedReader(new InputStreamReader(System.in));

  // Contains the Id for the last entered Account in SalesForce.
  private static String lstrAccountId;

  // Contains the Id for the last entered Contact in SalesForce.
  private static String lstrContactId;

  public ConnectToSalesForce() {
    int executionOption;
    do {
      showMenu();
      String choice = getUserInput("Enter option: ");
      executionOption = Integer.parseInt(choice);

      switch (executionOption) {
        case 1:
          // Login into SalesForce
          loginUsingOAuthForSalesForce();
          break;
        case 2:
          //Get List of Accounts from SalesForce
          getLastAddedAccount();
          break;
        case 3:
          // Get List of Contacts from SalesForce
          getLastAddedContact();
          break;
        case 4:
          // If an ccount id is not provided, 
          // Fetch the specific account and insert a new contact for that account.
          if (lstrAccountId == null || lstrAccountId.isEmpty()) {
            getLastAddedAccount();
          }
          this.insertNewContact(lstrAccountId);
          break;
        case 5:
          // If a contact id is not provided, fetch the last inserted contact id 
          // and update it with the given details. 
          if (lstrContactId == null || lstrContactId.isEmpty()) {
            this.getLastAddedContact();
          }
          this.updateContactBasedOnId(lstrContactId);
          break;
        case 6:
          restInsertDocument();
          break;
        case 7:
          // Subscribe to a topic and wait for response.
          StreamingClient lobjStreamingClient = new StreamingClient();
          break;
        case 99:
          exit(0);
          break;
        default:
          System.out.println("Please enter 1, 2, 3, 4, 5, 6, 7 or 99.\n");
          choice = "0";
          break;
      }
    } while (executionOption != Constants.NINETY_NINE);
    System.out.println("Program complete.");
  }

  /**
   * Displays the menu to perform actions on the SalesForce account.
   */
  private void showMenu() {
    System.out.println("\n \n 1. Login");
    System.out.println(" 2. Find Recently Added Account");
    System.out.println(" 3. Find Recently Added Contact");
    System.out.println(" 4. Insert Contact for Account");
    System.out.println(" 5. Update Contact");
    System.out.println(" 6. Insert Document");
    System.out.println(" 7. Subscribe to PushTopic");
    System.out.println("99. Exit \n");
  }

  /**
   * Logs into the Sales Force account using OAuth 2.0 Fetches the Token from
   * the response for subsequent requests
   */
  private void loginUsingOAuthForSalesForce() {
    System.out.println("_______________ Login _______________");
    try {
      String lstrPath = "https://"
        + Constants.USER_CREDENTIALS.loginInstanceDomain
        + Constants.OAUTH_ENDPOINT;

      // Login Auth details needs to be set in the URL
      // Note: Do not change to json, the request will not be authenticated.
      StringBuilder lstrbldrRequestBody = new StringBuilder("grant_type=password");
      lstrbldrRequestBody.append("&username=");
      lstrbldrRequestBody.append(Constants.USER_CREDENTIALS.userName);
      lstrbldrRequestBody.append("&password=");
      lstrbldrRequestBody.append(Constants.USER_CREDENTIALS.password);
      lstrbldrRequestBody.append("&client_id=");
      lstrbldrRequestBody.append(Constants.USER_CREDENTIALS.consumerKey);
      lstrbldrRequestBody.append("&client_secret=");
      lstrbldrRequestBody.append(Constants.USER_CREDENTIALS.consumerSecret);
      System.out.println("Request : " + lstrbldrRequestBody.toString());

      JSONObject ljsonResponse = RestCalls
        .RestPostWithFormEncoding(lstrPath, lstrbldrRequestBody.toString());
      OauthResponse.SetGlobalVariablesFromResponse(ljsonResponse);

      Constants.BASE_URI
        = Constants.INSTANCE_URL
        + Constants.REST_ENDPOINT
        + "/v" + Constants.USER_CREDENTIALS.apiVersion + ".0";

      Constants.OAUTH_HEADER = new BasicHeader("Authorization", "OAuth "
        + Constants.ACCESS_TOKEN);

      System.out.println("\nSuccessfully logged in to instance: " + Constants.BASE_URI);
    } catch (NullPointerException ioe) {
      ioe.printStackTrace();
    }
  }

  
   
  /**
   * Displays a message to the user and retrieves a user input
   *
   * @param lstrMessage : Contains the message to be displayed to the user for
   * an input.
   *
   * @return lstrUserInput : Returns the input entered by the user.
   */
  private String getUserInput(String lstrMessage) {
    String lstrUserInput = "";
    try {
      System.out.print(lstrMessage);
      lstrUserInput = INPUT_READER.readLine();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    return lstrUserInput;
  }

  /**
   * Fetches the Last added account from SalesForce
   */
  private void getLastAddedAccount() {
    System.out.println("\n_______________ Account QUERY _______________");
    try {
      String path = Constants.BASE_URI
        + "/query?q=SELECT+id+,+name+FROM+Account+ORDER+BY+id+DESC+LIMIT+1";

      lstrAccountId = RestCalls.RestGet(path,Constants.REQUESTING_CLASS.SALESFORCE)
        .getJSONArray("records").getJSONObject(0).getString("Id");
    } catch (JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
    System.out.println("Account Id value is " + lstrAccountId);
  }

  /**
   * Updates the Contact details based on a given Contact Id.
   *
   * @param pstrContactId : Contact Id for which details are to be updated.
   */
  private void updateContactBasedOnId(String pstrContactId) {
    System.out.println("\n_______________ Contact UPDATE _______________");

    // The Contact Id for the record to update is part of the URI, not part of the JSON
    String lstrPath = Constants.BASE_URI + "/sobjects/Contact/" + pstrContactId;
    try {
      //Create the JSON object containing the updated phone number
      JSONObject ljsonUpdateFields = new JSONObject();
      ljsonUpdateFields.put("Phone", "(415)555-1234");
      System.out.println("JSON for update of contact record:\n" + ljsonUpdateFields.toString(1));

      int lintStatusCode = RestCalls.RestPatch(lstrPath, ljsonUpdateFields);
      if (lintStatusCode == HttpStatus.SC_NO_CONTENT) {
        System.out.println("Updated the contact successfully.");
      } else {
        System.out.println("Contact update NOT successfully. Status code is " + lintStatusCode);
      }
    } catch (JSONException e) {
      System.out.println("Issue creating JSON or processing results");
      e.printStackTrace();
    } catch (NullPointerException ioe) {
      ioe.printStackTrace();
    }
  }

  /**
   * Inserts Contact Details to a given Account
   *
   * @param pstrAccountId : Contains the account id for which a contact is to be
   * added
   */
  private void insertNewContact(String pstrAccountId) {
    try {
      System.out.println("\n_______________ Contact INSERT _______________");

      String lstrPath = Constants.BASE_URI + "/sobjects/Contact/";

      //create the JSON object containing the new contacts details.
      JSONObject ljsonContactDetails = new JSONObject();
      System.out.println("Enter Last Name : ");
      ljsonContactDetails.put("LastName", INPUT_READER.readLine());
      System.out.println("Enter First Name : ");
      ljsonContactDetails.put("FirstName", INPUT_READER.readLine());
      System.out.println("Enter Mobile Number : ");
      ljsonContactDetails.put("MobilePhone", INPUT_READER.readLine());
      System.out.println("Enter Phone : ");
      ljsonContactDetails.put("Phone", INPUT_READER.readLine());
      
      ljsonContactDetails.put("AccountId", pstrAccountId);

      System.out.println("JSON for contact record to be inserted:\n" + ljsonContactDetails.toString(1));

      JSONObject ljsonResponse = RestCalls.RestPost(lstrPath,
        ljsonContactDetails, Constants.REQUESTING_CLASS.SALESFORCE);

      if (ljsonResponse != null) {
        lstrContactId = ljsonResponse.getString("id");
      }
      System.out.println("New contact id from response: " + lstrContactId);
    } catch (IOException | JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   * Fetched the Last added Contact from SalesForce
   */
  private void getLastAddedContact() {
    System.out.println("\n_______________ Contact QUERY _______________");
    try {
      String lstrPath = Constants.BASE_URI
        + "/query?q=SELECT+id+,+name+FROM+Contact+ORDER+BY+id+DESC+LIMIT+1";

      lstrContactId = RestCalls.RestGet(lstrPath,Constants.REQUESTING_CLASS.SALESFORCE)
        .getJSONArray("records").getJSONObject(0).getString("Id");
    } catch (JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
    System.out.println("Contact Id value is " + lstrContactId);

  }

  /**
   * Inserts a given document to SalesForce
   */
  private void restInsertDocument() {
    try {
     //  String lstrPathToDocument = "C:\\Users\\Saranya\\Desktop\\Sri Chandana Resume.pdf";
      String lstrPathToDocument = "C:\\Users\\ROCKERZ\\Desktop\\Salesforce3.pdf";
     // String lstrDocumentTitle = "Sri Chandana Resume.pdf";
      // String lstrDocumentDescription = "Testing document upload to SalesForce";
      String lstrDocumentTitle = "Salesforce3.pdf";
      String lstrDocumentDescription = "Testing1";

      // Read the file
      
      byte[] data = loadFileAsByteArray(lstrPathToDocument);

      JSONObject content = new JSONObject();
      content.put("PathOnClient", lstrPathToDocument);
      if (!lstrDocumentTitle.isEmpty()) {
        content.put("Title", lstrDocumentTitle);
      }
      if (!lstrDocumentDescription.isEmpty()) {
        content.put("Description", lstrDocumentDescription);
      }
      content.put("VersionData", new String(Base64.encodeBase64(data)));

      String lstrPath = Constants.BASE_URI + "/sobjects/ContentVersion/";

      JSONObject ljsonResponse = RestCalls.RestPost(lstrPath,
        content, Constants.REQUESTING_CLASS.SALESFORCE);
      String lstrContentId = ljsonResponse.getString("id");
      System.out.println("New Document/Content id from response: " + lstrContentId);
    } catch (JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   * Reads the document from a given path and converts it into bytes.
   *
   * @param pstrPathToDocument : Path at which the document is located.
   * @return data : Contains bytes to be uploaded to SalesForce.
   */
  private static byte[] loadFileAsByteArray(String pstrPathToDocument) {
    InputStream is = null;
    byte[] data = null;
    try {
      File file = new File(pstrPathToDocument);
      System.out.println("Is File ? : " + file.isFile());
      is = new FileInputStream(file);
      data = new byte[(int) file.length()];
      if (is.read(data) > 0) {
        return data;
      } else {
        System.out.println("No Data Found in the File");
        return data;
      }
    } catch (FileNotFoundException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      try {
        if (is != null) {
          is.close();
        }
      } catch (IOException ex) {
        Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return data;
  }

  /**
   * Validates the choice of number entered by the user and returns the correct
   * choice
   *
   * @param pstrChoice : Number entered by the user
   */
  private int ValidateChoice(String pstrChoice) {
    int executionOption;
    boolean invalidValue = true;
    while (invalidValue) {
      try {
        executionOption = Integer.parseInt(pstrChoice);
        if ((executionOption < 1 || executionOption > 7)
          && executionOption != Constants.NINETY_NINE) {
          System.out.println("Please enter 1, 2, 3, 4, 5, 6, 7 or 99.\n");
          pstrChoice = getUserInput("Enter the number of the sample to run: ");

        } else {
          invalidValue = false;
        }
      } catch (Exception e) {
        System.out.println("Invalid value. Please enter 1, 2, 3, 4, 5, 6, 7 or 99.\n");
        pstrChoice = getUserInput("Enter the number of the sample to run: ");
      }
    }
    return Integer.parseInt(pstrChoice);
  }
  
}
