package SyncTool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Contains the GET, POST and PATCH functions which can be used to make RESTful
 * calls.
 *
 * @author Saranya
 */
public class RestCalls {

  /**
   * Retrieves a JSON response from the give URL
   *
   * @param pstrPath: Request URL from which data is to be retrieved.
   * @param lenumRequestingClass
   * @return : JSON response received from the REST URL.
   */
  public static JSONObject RestGet(String pstrPath,
    Constants.REQUESTING_CLASS lenumRequestingClass) {
    try {
      JSONObject lobjJsonObject = null;
      System.out.println("Query URL: " + pstrPath);

      HttpGet lobjHttpGet = new HttpGet(pstrPath);
      switch (lenumRequestingClass) {
        case SALESFORCE:
          lobjHttpGet.addHeader(Constants.OAUTH_HEADER);
          lobjHttpGet.addHeader(Constants.PRETTY_PRINT_HEADER);
          break;
        case MIP:
          lobjHttpGet.addHeader(Constants.MIP_AUTH_HEADER);
          lobjHttpGet.addHeader(new BasicHeader("Accept", "application/json"));
          break;
      }

      // Make the request.
      HttpResponse lobjHttpResponse = new DefaultHttpClient().execute(lobjHttpGet);

      // Process the result
      int lintStatusCode = lobjHttpResponse.getStatusLine().getStatusCode();
      if (lintStatusCode == HttpStatus.SC_OK) {
        String lstrResponseString = EntityUtils.toString(lobjHttpResponse.getEntity());
        try {
          lobjJsonObject = new JSONObject(lstrResponseString);
          System.out.println("JSON result of Query:\n" + lobjJsonObject.toString(1));
        } catch (JSONException je) {
          je.printStackTrace();
        }
      } else {
        System.out.println("Query was unsuccessful. Status code returned is " + lintStatusCode);
      }
      return lobjJsonObject;
    } catch (IOException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
  }
  
  public static String RestGetDocument(String pstrPath,
    Constants.REQUESTING_CLASS lenumRequestingClass) {
    try {
        String lstrResponseString =null;
//     JSONArray array = null;
      System.out.println("Query URL: " + pstrPath);

      HttpGet lobjHttpGet = new HttpGet(pstrPath);
      switch (lenumRequestingClass) {
        case SALESFORCE:
          lobjHttpGet.addHeader(Constants.OAUTH_HEADER);
          lobjHttpGet.addHeader(Constants.PRETTY_PRINT_HEADER);
          break;
        case MIP:
          lobjHttpGet.addHeader(Constants.MIP_AUTH_HEADER);
          lobjHttpGet.addHeader(new BasicHeader("Accept", "application/json"));
          break;
      }

      // Make the request.
      HttpResponse lobjHttpResponse = new DefaultHttpClient().execute(lobjHttpGet);

      // Process the result
      int lintStatusCode = lobjHttpResponse.getStatusLine().getStatusCode();
      if (lintStatusCode == HttpStatus.SC_OK) {
           lstrResponseString = EntityUtils.toString(lobjHttpResponse.getEntity());
           
      } else {
        System.out.println("Query was unsuccessful. Status code returned is " + lintStatusCode);
      }
      return lstrResponseString;
    } catch (IOException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
  }

  /**
   * Inserts values at the given REST URL using JSON object.
   *
   * @param pstrPath : Request URL at which data is to be inserted.
   * @param pjsonValuesToPost : JSON object containing values to be inserted.
   * @param lenumRequestingClass
   * @return : JSON response received from the REST URL.
   */
 /* 
  public static JSONObject RestPost(String pstrPath,
    JSONObject pjsonValuesToPost, Constants.REQUESTING_CLASS lenumRequestingClass) {
    JSONObject ljsonResponse = null;
    try {
      //Construct the objects needed for the request
      DefaultHttpClient lhttpclientObject = new DefaultHttpClient();
      HttpPost lhttppostObject = new HttpPost(pstrPath);

      switch (lenumRequestingClass) {
        case SALESFORCE:
          lhttppostObject.addHeader(Constants.OAUTH_HEADER);
          lhttppostObject.addHeader(Constants.PRETTY_PRINT_HEADER);
          break;
        case MIP:
             lhttppostObject.addHeader(Constants.MIP_AUTH_HEADER);
          lhttppostObject.addHeader(new BasicHeader("Accept", "application/json"));
        default:
          break;
      }
      // The message we are going to post
      StringEntity lstrRequestBody = new StringEntity(pjsonValuesToPost.toString(1));
      lstrRequestBody.setContentType("application/json");
      lhttppostObject.setEntity(lstrRequestBody);

      //Make the request
      HttpResponse lhttpresponseObject = lhttpclientObject.execute(lhttppostObject);

      //Process the results
      int lintResponseStatusCode = lhttpresponseObject.getStatusLine().getStatusCode();
      switch (lintResponseStatusCode) {
        case HttpStatus.SC_OK:
        case HttpStatus.SC_CREATED:
          String response_string = EntityUtils.toString(lhttpresponseObject.getEntity());
          ljsonResponse = new JSONObject(response_string);
          break;
        default:
          switch (lenumRequestingClass) {
            case SALESFORCE:
              System.out.println("Insertion unsuccessful. Status code returned is "
                + lintResponseStatusCode);
              break;
            case MIP:
              System.out.println("Login unsuccessful. Status code returned is "
                + lintResponseStatusCode);
              break;
          }
          break;
      }
    } catch (JSONException | IOException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
    }
    return ljsonResponse;
  }
  */
  public static JSONObject RestPost(String pstrPath,
    JSONObject pjsonValuesToPost, Constants.REQUESTING_CLASS lenumRequestingClass) {
    JSONObject ljsonResponse = null;
    try {
      //Construct the objects needed for the request
      DefaultHttpClient lhttpclientObject = new DefaultHttpClient();
      HttpPost lhttppostObject = new HttpPost(pstrPath);

      switch (lenumRequestingClass) {
        case SALESFORCE:
          lhttppostObject.addHeader(Constants.OAUTH_HEADER);
          lhttppostObject.addHeader(Constants.PRETTY_PRINT_HEADER);
          break;
        case MIP:
             lhttppostObject.addHeader(Constants.MIP_AUTH_HEADER);
          lhttppostObject.addHeader(new BasicHeader("Accept", "application/json"));
        default:
          break;
      }
      
     String methodName = (String) pjsonValuesToPost.get("method");
     if(pjsonValuesToPost.has("method")){
         pjsonValuesToPost.remove("method");
     }
        System.out.println(pjsonValuesToPost.toString());
      // The message we are going to post
      StringEntity lstrRequestBody = new StringEntity(pjsonValuesToPost.toString(1));
      lstrRequestBody.setContentType("application/json");
      lhttppostObject.setEntity(lstrRequestBody);
 
      //Make the request
      HttpResponse lhttpresponseObject = lhttpclientObject.execute(lhttppostObject);

      //Process the results
      int lintResponseStatusCode = lhttpresponseObject.getStatusLine().getStatusCode();
      switch (lintResponseStatusCode) {
        case HttpStatus.SC_OK:
        case HttpStatus.SC_CREATED:
             String response_string = "";
            if(methodName.equals("Session")){
                JSONObject success = new JSONObject();
                success.put("success", "created");
                response_string= success.toString();
            }
            else if (methodName.equals("document")){
            JSONObject success = new JSONObject();
                success.put("success", "created");
                response_string= success.toString();
            }
            else {
          response_string = EntityUtils.toString(lhttpresponseObject.getEntity());
          
      }
          ljsonResponse = new JSONObject(response_string);
         //   System.out.println("Session successful");
          break;
        default:
          switch (lenumRequestingClass) {
            case SALESFORCE:
              System.out.println("Insertion unsuccessful. Status code returned is "
                + lintResponseStatusCode);
              break;
            case MIP:
              System.out.println("Login unsuccessful. Status code returned is "
                + lintResponseStatusCode);
              break;
          }
          break;
      }
    } catch (JSONException | IOException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
    }
    return ljsonResponse;
  } 

  /**
   * Inserts values at the given REST URL using Form URL Encoding. This method
   * is used during the login OAuth process.
   *
   * @param pstrPath : Request URL at which data is to be inserted.
   * @param pstrValuesToPost : Form encoding values needed for Authentication.
   * @return : JSON response received from the REST URL.
   */
  public static JSONObject RestPostWithFormEncoding(String pstrPath, String pstrValuesToPost) {
    JSONObject ljsonResponse = null;
    try {
      HttpClient lhttpclientObject = new DefaultHttpClient();
      HttpPost lhttppostObject = new HttpPost(pstrPath);

      StringEntity lstrRequestBody = new StringEntity(pstrValuesToPost);
      lstrRequestBody.setContentType("application/x-www-form-urlencoded");
      lhttppostObject.setEntity(lstrRequestBody);

      lhttppostObject.addHeader(Constants.PRETTY_PRINT_HEADER);

      //Make the request and store the result
      HttpResponse lhttpresponseObject = lhttpclientObject.execute(lhttppostObject);

      if (lhttpresponseObject.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        String lstrResponseString = EntityUtils.toString(lhttpresponseObject.getEntity());
        try {
          ljsonResponse = new JSONObject(lstrResponseString);
          System.out.println("JSON returned by response: \n" + ljsonResponse.toString(1));
        } catch (JSONException je) {
          je.printStackTrace();
        }
      } else {
        System.out.println("An error has occured. Http status: " + lhttpresponseObject.getStatusLine().getStatusCode());
        System.out.println(formatInputStream(lhttpresponseObject.getEntity().getContent()));
        System.exit(-1);
      }
    } catch (UnsupportedEncodingException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
    }
    return ljsonResponse;
  }

  /**
   * Updates values at the given REST URL using JSON object.
   *
   * @param pstrPath : Request URL at which data is to be updated.
   * @param pjsonValuesToUpdate : JSON object containing values to be updated.
   * @return : Status code which specifies if the update was successful.
   */
  public static int RestPatch(String pstrPath, JSONObject pjsonValuesToUpdate) {
    try {
      //Set up the objects necessary to make the request.
      DefaultHttpClient httpClient = new DefaultHttpClient();
      HttpPatch httpPatch = new HttpPatch(pstrPath);
      httpPatch.addHeader(Constants.OAUTH_HEADER);
      httpPatch.addHeader(Constants.PRETTY_PRINT_HEADER);
      StringEntity body = new StringEntity(pjsonValuesToUpdate.toString(1));
      body.setContentType("application/json");
      httpPatch.setEntity(body);

      //Make the request
      HttpResponse response = httpClient.execute(httpPatch);

      //Process the response
      return response.getStatusLine().getStatusCode();
    } catch (IOException | JSONException ex) {
      Logger.getLogger(RestCalls.class.getName()).log(Level.SEVERE, null, ex);
    }
    return -1;
  }

  /**
   * Reads a stream of input and converts it into displayable format by
   * inserting line feed.
   *
   * @param pinputstreamReceived : Stream of data received in a single line.
   * @return lstrFinalInput : Formatted data with line feed.
   */
  private static String formatInputStream(InputStream pinputstreamReceived) {
    String lstrFinalInput = "";
    try {
      try (BufferedReader lbuffReader = new BufferedReader(
        new InputStreamReader(pinputstreamReceived)
      )) {
        String lstrInputLine;
        while ((lstrInputLine = lbuffReader.readLine()) != null) {
          lstrFinalInput += lstrInputLine;
          lstrFinalInput += "\n";
        }
      }
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    return lstrFinalInput;
  }

    private static JSONArray JSONArray(String lstrResponseString) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
