/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SyncTool;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

/**
 *
 * @author Saranya
 */
public class Constants {

  public static final String OAUTH_ENDPOINT = "/services/oauth2/token";
  public static final String REST_ENDPOINT = "/services/data";
  
  public static final UserCredentials USER_CREDENTIALS = new UserCredentials();
  
  public static String USER_ID;
  public static String ISSUED_AT;
  public static String INSTANCE_URL;
  public static String SIGNATURE;
  public static String ACCESS_TOKEN;

  public static String BASE_URI;
  public static Header PRETTY_PRINT_HEADER = new BasicHeader("X-PrettyPrint", "1");
  public static Header OAUTH_HEADER;
  
  public enum TYPE_OF_REQUEST_IN_THE_BODY{
    FORM_URL_ENCODE,
    JSON
  };
  
  public static final int NINETY_NINE = 99;

  //Constants for Mip
  public static final String MIP_BASE_URI = "http://ec2-52-89-3-34.us-west-2.compute.amazonaws.com:9001";
  public static String MIP_TOKEN;
  public static Header MIP_AUTH_HEADER;
  
  public enum REQUESTING_CLASS{
    SALESFORCE,
    MIP
  };
}
