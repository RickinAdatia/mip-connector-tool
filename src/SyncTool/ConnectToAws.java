/**
 * Reference for Code
 * 1. http://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc
 *          /com/amazonaws/services/ec2/AmazonEC2Client.html
 * 2. http://stackoverflow.com/questions/23081848
 *          /get-instance-id-of-ec2-instance-via-java
 */
package SyncTool;

/* Importing java specific packages */
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* Importing AWS specific packages */ 
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;

/**
 * Contains functions used to Connect to an AWS account specified in the
 * credentials file, Fetches all the instances from the AWS account and Helps to
 * lookup a specific instance in the AWS account.
 *
 * @author Saranya
 */
public class ConnectToAws {

  // Used to connect to a specific amazon ec2 account
  // and fetch details from the account.
  private static AmazonEC2 lobjAmazonEc2;

  /**
   * Used to Login into an AWS account specified in the credentials file.
   */
  protected static void LoginIntoAws() {

    AWSCredentials lobjAwsCredentials = null;
    try {
      lobjAwsCredentials = new ProfileCredentialsProvider(
        "src\\SyncTool\\credentials", "default")
        .getCredentials();
    } catch (Exception e) {
      System.out.println(e);
      throw new AmazonClientException(
        "Cannot load the credentials from the credential profiles file.  "
        + "Please make sure that your credentials file is at the correct "
        + "location (credentials), and is in valid format.",
        e);
    }
    if (lobjAwsCredentials != null) {
      lobjAmazonEc2 = new AmazonEC2Client(lobjAwsCredentials);
      lobjAmazonEc2.setEndpoint("https://ec2.us-west-2.amazonaws.com");
    }
  }

  /**
   * Fetches the details of an instance(s) based on the instance id(s).
   *
   * @param pstrInstanceId : Contains an instance id for fetching instance
   * details
   */
  protected static void FetchAnInstanceBasedOnId(String pstrInstanceId) {
    try {
      // Contains a list of Instance Id's for which details are to be fetched
      List<String> llststrServiceInstanceIds = new ArrayList<>();

      // If more than 1 instance id is supplied split it based on a character
      // Else add the given instance id to the list for fetching details.
      if (!pstrInstanceId.isEmpty()) {
        String[] larInstances = pstrInstanceId.split("\\|");
        int lintLength = larInstances.length;
        if (lintLength > 1) {
          for (int count = lintLength; count > 0; count--) {
            llststrServiceInstanceIds.add(larInstances[count - 1].trim());
          }
        } else if (lintLength == 1) {
          llststrServiceInstanceIds.add(pstrInstanceId);
        }
      }

      // Create a request object to fetch the instance details.
      DescribeInstancesRequest lobjInstanceRequest
        = new DescribeInstancesRequest();
      lobjInstanceRequest.setInstanceIds(llststrServiceInstanceIds);

      // Fetch the results of the request for instance details.
      DescribeInstancesResult lobjInstanceDetails
        = lobjAmazonEc2.describeInstances(lobjInstanceRequest);

      // Get the list of Reservations 
      // From each reservation get the list of instance
      // From each instance fetch the required details
      List<Reservation> llstReservations
        = lobjInstanceDetails.getReservations();
      llstReservations.stream().forEach((lobjReservation) -> {
        lobjReservation.getInstances().stream().forEach((instance) -> {
          System.out.println(instance.getPublicDnsName());
        });
      });
    } catch (Exception e) {
      System.out.println("Instance Test " + e);
    }
  }

  /**
   * Fetches the details of all Instances within the AWS account.
   */
  protected static void FetchAllInstances() {
    try {
      // Create a request object to fetch the instance details.
      DescribeInstancesRequest lobjInstanceRequest = new DescribeInstancesRequest();

      /*
      // Used to fetch instances based on the Key-value Tag 
      // added to the instance while creating the instance.
      List<String> valuesT1 = new ArrayList<>();
      valuesT1.add("Administrators");
      
      // Extracts instances with Key value pair added in list 
      Filter filter = new Filter("key-name", valuesT1);
       */
      // Fetch the results of the request for instance details
      // based on the filter.
      DescribeInstancesResult lobjInstanceDetails
        = lobjAmazonEc2.describeInstances(
          lobjInstanceRequest.withFilters(new Filter()));

      List<Reservation> reservations = lobjInstanceDetails.getReservations();
      Set<Instance> lsetInstances = new HashSet<>();

      reservations.stream().forEach((reservation) -> {
        lsetInstances.addAll(reservation.getInstances());
      });

      System.out.println("You have " + lsetInstances.size() + " Amazon EC2 instance(s) running.\n");
      lsetInstances.stream().forEach((lobjInstance) -> {
        System.out.println(lobjInstance.getInstanceId()
          + "\t" + lobjInstance.getPublicDnsName());
      });

    } catch (AmazonServiceException ase) {
      System.out.println("Caught Exception: " + ase.getMessage());
      System.out.println("Reponse Status Code: " + ase.getStatusCode());
      System.out.println("Error Code: " + ase.getErrorCode());
      System.out.println("Request ID: " + ase.getRequestId());
    } catch (Exception e) {
      System.out.println("Testing exception" + e);
    }
  }
}
