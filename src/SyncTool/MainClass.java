package SyncTool;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The main class which makes function calls to other classes.
 *
 * @author Saranya
 */
public class MainClass {

  /**
   * Entry point of the program. Contains the function calls for Logging into 
   * AWS And menu for operations on SalesForce.
   *
   * @param args
   */
  public static void main(String[] args) {
    try {
     //  Connect to AWS
      ConnectToAws.LoginIntoAws();

      // Fetch all Instance from AWS
      System.out.println("All Instances\n");
      ConnectToAws.FetchAllInstances();

      // Fetch details of selective instances from AWS 
      System.out.println("Filter Based\n");
      ConnectToAws.FetchAnInstanceBasedOnId("i-defa7c28");// |i-03a92a97

     ConnectToMip lobjMip = new ConnectToMip();
       lobjMip.LoginIntoMip();
//       lobjMip.FetchDistributionCode();
  //     lobjMip.FetchSessionDocumentIDS();
  //     lobjMip.InsertSession();
      //lobjMip.FetchChartOfAccounts();
      // lobjMip.FetchListOfSessions();
 //    lobjMip.createDocument();
  //  lobjMip.InsertDocument();
      // Login into SalesForce
     ConnectToSalesForce lobjLoginUsingOAuth = new ConnectToSalesForce();

        lobjLoginUsingOAuth = null;
      
      

      System.out.println("No Error in previous line");
    } catch (Exception ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}