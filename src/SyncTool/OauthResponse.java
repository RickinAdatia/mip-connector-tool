package SyncTool;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used to set the details received in the Response into global variables
 *
 * @author Saranya
 */
public class OauthResponse {

  public static void SetGlobalVariablesFromResponse(JSONObject json) {
    try {
      Constants.USER_ID = json.getString("id");
      Constants.ISSUED_AT = json.getString("issued_at");
      Constants.INSTANCE_URL = json.getString("instance_url");
      Constants.SIGNATURE = json.getString("signature");
      Constants.ACCESS_TOKEN = json.getString("access_token");

    } catch (JSONException e) {
      e.printStackTrace();
    }
  }
}
