    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SyncTool;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.message.BasicHeader;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import static org.json.JSONObject.NULL;

/**
 *
 * @author Saranya
 */
public class ConnectToMip {

  /**
   * Logs into MIP using REST POST call.
   */
  public void LoginIntoMip() {
    try {
      String lstrPath = Constants.MIP_BASE_URI + "/api/security/login";

      System.out.println("Path for Login :\n " + lstrPath);

      //create the JSON object containing the login details.
      JSONObject ljsonLoginCredentials = new JSONObject();
      ljsonLoginCredentials.put("login", "admin");
      ljsonLoginCredentials.put("org", "NTO");
      ljsonLoginCredentials.put("password", "api");
      ljsonLoginCredentials.put("method", "Login");

      System.out.println("JSON for Login into MIP :\n" + ljsonLoginCredentials.toString(1));

      JSONObject ljsonResponse = RestCalls.RestPost(lstrPath,
        ljsonLoginCredentials, Constants.REQUESTING_CLASS.MIP);

      if (ljsonResponse != null) {
        Constants.MIP_TOKEN = ljsonResponse.getString("token");
      }
      System.out.println("Token from response: " + Constants.MIP_TOKEN);
      Constants.MIP_AUTH_HEADER = new BasicHeader("Authorization-Token",
        Constants.MIP_TOKEN);

    } catch (JSONException ex) {
      Logger.getLogger(ConnectToMip.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   *
   */
  public void FetchDistributionCode() {
    try {
      String lstrPath = Constants.MIP_BASE_URI + "/api/dc";

      JSONObject ljsonDistributionCodes = RestCalls.RestGet(lstrPath,
        Constants.REQUESTING_CLASS.MIP);
      System.out.println("Distribution Code List :\n"
        + ljsonDistributionCodes.toString(1));

    } catch (JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  public String FetchSessionDocumentIDS() {
    
      String lstrPath = Constants.MIP_BASE_URI + "/api/te/JV/sessions/200/documents/ids";

      String docIds = RestCalls.RestGetDocument(lstrPath,
        Constants.REQUESTING_CLASS.MIP);
      docIds = docIds.replace("[", "").replace("]", "");
      System.out.println("Session Document IDs :\n"
        + docIds);

    return docIds;
  }
  
  public void FetchChartOfAccounts() {
    try {
      String lstrPath = Constants.MIP_BASE_URI + "/api/coa/segments";

      JSONObject ljsonChartOfAccounts = RestCalls.RestGet(lstrPath,
        Constants.REQUESTING_CLASS.MIP);
      System.out.println("Chart Of Accounts :\n"
        + ljsonChartOfAccounts.toString(1));

    } catch (JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  public JSONObject FetchListOfSessions() {
    try {
      String lstrPath = Constants.MIP_BASE_URI + "/api/te/JV/sessions";

      JSONObject ljsonListOfSessions = RestCalls.RestGet(lstrPath,
        Constants.REQUESTING_CLASS.MIP);
      System.out.println("List Of Sessions :\n"
        + ljsonListOfSessions.toString(1));
return ljsonListOfSessions;
    } catch (JSONException ex) {
      Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
       return null;
    }
   
  }
public void InsertSession() {
    try {
      String lstrPath = Constants.MIP_BASE_URI + "/api/te/JV/sessions";

      System.out.println("Path for creation of session :\n " + lstrPath);
      
      JSONArray json = new JSONArray();
     // JSONObject object = new JSONObject();
      JSONObject Final = new JSONObject();
      JSONObject A1 = new JSONObject();
      A1.put("SESSION_SESSIONNUMID" , "200");
      
      JSONObject A2 = new JSONObject();
      A2.put("SESSION_STATUS" , "BP");
      
      JSONObject A3 = new JSONObject();
      A3.put("SESSION_DESCRIPTION" , "CODING");
      
      JSONObject A4 = new JSONObject();
      A4.put("SESSION_SESSIONDATE" , "2016-10-20");
      
      JSONObject A5 = new JSONObject();
      A5.put("SESSION_CURRENCY_TYPE" , "USD");
    //  object.put("def", array);
    json.put(A1);
     json.put(A2);
      json.put(A3);
       json.put(A4);
        json.put(A5);
       
      Final.put("fields",json); 
      Final.put("method", "Session");
      
      System.out.println("Passing request :\n" + Final.toString(1));

      JSONObject ljsonResponse = RestCalls.RestPost(lstrPath,
        Final, Constants.REQUESTING_CLASS.MIP);

//      if (ljsonResponse != null) {
//        Constants.MIP_TOKEN = ljsonResponse.getString("token");
//      }
//      System.out.println("Token from response: " + Constants.MIP_TOKEN);
//      Constants.MIP_AUTH_HEADER = new BasicHeader("Authorization-Token",
//        Constants.MIP_TOKEN);
      System.out.println(" Session response: " + ljsonResponse.get("success") );


    } catch (JSONException ex) {
      Logger.getLogger(ConnectToMip.class.getName()).log(Level.SEVERE, null, ex);
    }
  }


public void InsertDocument(JSONObject obj ) {
    try {
      String lstrPath = Constants.MIP_BASE_URI + "/api/te/JV/sessions/200/documents";

      System.out.println("Path for creation of Document :\n " + lstrPath);
      

		JSONObject B1 = new JSONObject();
		B1.put("ClientNumbers" , 0);
		JSONObject B2 = new JSONObject();
		B2.put("WorkerHours", 0);
		JSONArray json = new JSONArray();
		json.put(B1);
		json.put(B2);
	
		JSONObject B3 = new JSONObject();
		B3.put("TETRANS_SEGMENT_0", "40001");
		JSONObject B4 = new JSONObject();
		B4.put("TETRANS_SEGMENT_1", "01");
		JSONObject B5 = new JSONObject();
		B5.put("TETRANS_SEGMENT_2","301");
		JSONObject B6 = new JSONObject();
		B6.put("TETRANS_SEGMENT_3", "401");
		JSONObject B7 = new JSONObject();
		B7.put("TETRANS_SEGMENT_4","201");
		JSONObject B8 = new JSONObject();
		B8.put("TETRANS_SEGMENT_5","1");
		JSONArray json2 = new JSONArray();
		json2.put(B3);
		json2.put(B4);
		json2.put(B5);
		json2.put(B6);
		json2.put(B7);
		json2.put(B8);
	
                JSONObject data = obj.getJSONObject("data");
                JSONObject sObject = data.getJSONObject("sobject");
                
                
  String lldate =  sObject.get("npe01__Payment_Amount__c").toString();



                
		JSONObject B9 = new JSONObject();
		B9.put("TETRANS_EFFECTIVEDATE",sObject.get("npe01__Payment_Date__c") );
		JSONObject B10 = new JSONObject();
		B10.put("TETRANS_ENTRY_TYPE", "N");
		JSONObject B11 = new JSONObject();
		B11.put("TETRANS_DESCRIPTION",sObject.get("npe01__Opportunity__c"));
		JSONObject B12 = new JSONObject();
		B12.put("TETRANS_SRC_DEBIT","0");
		JSONObject B13 = new JSONObject();
		B13.put("TETRANS_SRC_CREDIT",sObject.get("npe01__Payment_Amount__c"));
		JSONArray json3 =  new JSONArray();
		json3.put(B9);
		json3.put(B10);
		json3.put(B11);
		json3.put(B12);
		json3.put(B13);
		
		JSONObject Final = new JSONObject();
               Final.put("UserDefinedFields", json);
               Final.put("segments", json2);
               Final.put("fields", json3);
		
		JSONObject B14 = new JSONObject();
		B14.put("ClientNumbers" , 0);
		JSONObject B15 = new JSONObject();
		B15.put("WorkerHours", 0);
		JSONArray json4 = new JSONArray();
		json4.put(B14);
		json4.put(B15);
	
		JSONObject B16 = new JSONObject();
		B16.put("TETRANS_SEGMENT_0", "11000");
		JSONObject B17 = new JSONObject();
		B17.put("TETRANS_SEGMENT_1", "01");
		JSONObject B18 = new JSONObject();
		B18.put("TETRANS_SEGMENT_2","301");
		JSONObject B19 = new JSONObject();
		B19.put("TETRANS_SEGMENT_3", "401");
		JSONObject B20 = new JSONObject();
		B20.put("TETRANS_SEGMENT_4","201");
		JSONObject B21 = new JSONObject();
		B21.put("TETRANS_SEGMENT_5","1");
		JSONArray json5 = new JSONArray();
		json5.put(B16);
		json5.put(B17);
		json5.put(B18);
		json5.put(B19);
		json5.put(B20);
		json5.put(B21);
	
		JSONObject B22 = new JSONObject();
		B22.put("TETRANS_EFFECTIVEDATE",sObject.get("npe01__Payment_Date__c"));
		JSONObject B23 = new JSONObject();
		B23.put("TETRANS_ENTRY_TYPE", "N");
		JSONObject B24 = new JSONObject();
		B24.put("TETRANS_DESCRIPTION",sObject.get("npe01__Opportunity__c"));
		JSONObject B25 = new JSONObject();
		B25.put("TETRANS_SRC_DEBIT",sObject.get("npe01__Payment_Amount__c"));
		JSONObject B26 = new JSONObject();
		B26.put("TETRANS_SRC_CREDIT","0");
		JSONArray json6 =  new JSONArray();
		json6.put(B22);
		json6.put(B23);
		json6.put(B24);
		json6.put(B25);
		json6.put(B26);
		
               JSONObject Final2 = new JSONObject();
               Final2.put("UserDefinedFields", json4);
               Final2.put("segments", json5);
               Final2.put("fields", json6);
               
               JSONArray json7 = new JSONArray();
               json7.put(Final);
               json7.put(Final2);
               
               JSONObject Final3 = new JSONObject();
               Final3.put("transactions", json7);
		
		JSONObject B27 = new JSONObject();
		B27.put("TEDOC_DOCNUM",sObject.get("npe01__Check_Reference_Number__c"));
		JSONObject B28 = new JSONObject();
		B28.put("TEDOC_DESCRIPTION",sObject.get("npe01__Opportunity__c"));
		JSONObject B29 = new JSONObject();
		B29.put("TEDOC_DOCDATE",sObject.get("npe01__Payment_Date__c"));
		JSONArray json8 =new JSONArray();
		json8.put(B27);
		json8.put(B28);
		json8.put(B29);
		JSONObject Final8 = new JSONObject();
		Final8.put("fields", json8);
                
                JSONObject Final4 = new JSONObject();
                Final4.put("transactions",json7 );
                Final4.put("fields", json8);
                
      Final4.put("method", "document");
      
      
      System.out.println("Passing request :\n" + Final4.toString());

      JSONObject ljsonResponse = RestCalls.RestPost(lstrPath,
        Final4, Constants.REQUESTING_CLASS.MIP);

     if (ljsonResponse != null) {
        Constants.MIP_TOKEN = ljsonResponse.getString("token");
      }
     System.out.println("Token from response: " + Constants.MIP_TOKEN);
      Constants.MIP_AUTH_HEADER = new BasicHeader("Authorization-Token",
       Constants.MIP_TOKEN);
      System.out.println("Session response: " + ljsonResponse.get("success") );


    } catch (JSONException ex) {
      Logger.getLogger(ConnectToMip.class.getName()).log(Level.SEVERE, null, ex);
    }
  }


  public void createDocument(JSONObject obj){
  JSONObject json = FetchListOfSessions();
      try {
           JSONArray sessionList  = json.getJSONArray("SESSION_SESSIONNUMID");
          for(int i=0; i<sessionList.length(); i++){
               JSONObject session =  (JSONObject) sessionList.get(i);
              String sessionId = session.get("SESSION_SESSIONNUMID").toString();
              if(sessionId.equals("200")){
                  InsertDocument(obj);
                  break;
              }
              else{
                  InsertSession();
                  break;
              }
          } 
          }
       catch (JSONException ex) {
          Logger.getLogger(ConnectToMip.class.getName()).log(Level.SEVERE, null, ex);
      }
  }
  
  
  } 

  